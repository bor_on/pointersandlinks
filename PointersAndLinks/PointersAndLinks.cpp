#include <iostream>
#include <stack>

class Stack
{
private:
	int n;
	int* steck;
	int i = -1;
public:
	Stack()
	{
		std::cout << "Massive size is: ";
		std::cin >> n;
		steck = new int[n];
	}
	void pop()
	{
		std::cout << "getting top element: " << steck[i] << "\n";
		i--;
	}
	void push()
	{
		int a;
		std::cout << "enter new element:";
		std::cin >> a;
		steck[++i] = a;
		std::cout << "new element is: " << steck[i] << "\n";
	}
	void print()
	{
		for (int i = 0; i < n; i++)
		{
			std::cout << steck[i] << ' ';
		}
		std::cout << "\n";
	}
	~Stack()
	{
		delete[] steck;
	}
};

int main()
{
	Stack mass;
	mass.push();
	mass.push();
	mass.pop();
	mass.print();
	mass.push();
	mass.print();
}